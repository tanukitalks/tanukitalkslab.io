# Adding a new episode

1. Create a new `.md` file in the **_posts** folder
1. Add the required frontmatter:
  - `title:` - A short title for the post (displayed on card and page)
  - `shortdesc:` - A short description (displayyed on card)GitLab values and how they've impacted our work and lives!
  - `homepic` - square picture for home card
  - `style:` -  "style1"-"style5" for posts or "styleComingSoon" for coming soon