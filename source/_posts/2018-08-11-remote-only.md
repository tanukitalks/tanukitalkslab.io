---
layout:     post
date:       2018-08-11
title: "#2: Remote work - How do?"
categories: podcast
tags:
- values
- gitlab
podcast_link: http://feeds.soundcloud.com/stream/486339399-tanukitalks-2-lets-talk-gitlab-values.mp3
podcast_file_size: 97.5 MB
podcast_duration: "47:59"
podcast_length: 69106999
homepic: avi-richards-183715-unsplash.jpg
shortdesc: We talk about being a 100% remote _only_ company and what that means to the way we work.
soundcloudid:   486339399
style: style2
---


### Links from the episodes

* [RemoteOnly.org](https://remoteonly.org)
* [GitLab remote only resources](https://about.gitlab.com/culture/remote-only/)
* [Territorial claims in Antarctica](https://en.wikipedia.org/wiki/Territorial_claims_in_Antarctica)
* [Remote work done right](https://about.gitlab.com/2018/03/16/remote-work-done-right/)