---
layout:     post
date:       2018-08-05
title:      "#1: Let's talk GitLab values"
categories: podcast
tags:
- values
- gitlab
podcast_link: http://feeds.soundcloud.com/stream/482313393-tanukitalks-1-lets-talk-gitlab-values.mp3
podcast_file_size: 97.5 MB
podcast_duration: "1:06:51"
podcast_length: 96282648
homepic:        danielle-macinnes-222441-unsplash.png
shortdesc:  Welcome to Tanuki Talks!  We'll start by talking about the GitLab values and how they've impacted our work and lives!
soundcloudid:   482313393
style: style5
---

### Welcome to Tanuki Talks!

In this first episode, we dive into the GitLab Values.  Luca, David, and Brendan talk about how those values have impacted their life both at work and at home.  Also, conflict arises based on stories from a galaxy far far away.

### Links from the episode

* [GitLab Values](https://about.gitlab.com/handbook/values/)
* [GitLab Jobs Page](https://about.gitlab.com/jobs/)
* [Diagram of building a car for a customer](https://about.gitlab.com/handbook/product/#the-minimally-viable-change)
* [Merge request changing the jobs page](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/12696)
* [Merge request we made on the podcast!](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/13226)