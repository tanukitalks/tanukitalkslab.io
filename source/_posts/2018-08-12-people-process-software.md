---
layout:     post
date:       2018-08-12
title: "#3: People, Process and Tools"
categories: podcast
tags:
- values
- gitlab
podcast_link: http://feeds.soundcloud.com/stream/486428061-tanukitalks-3-people-process-and-gitlab-tools.mp3
podcast_file_size: 108.8 MB
podcast_duration: "1:16:17"
podcast_length: 62562701
homepic: ashim-d-silva-106275-unsplash.jpg
shortdesc: How do people and processes fit into tools, or the other way around?  How important is each?
soundcloudid:   486428061
style: style1
---

### Links from the episodes

* [Cartoon episode idea](https://gitlab.com/tanukitalks/tanukitalks.gitlab.io/issues/22)